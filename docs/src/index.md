```@meta
CurrentModule = SVMakieRecipes
```

# SVMakieRecipes

Documentation for [SVMakieRecipes](https://github.com/mcamp/SVMakieRecipes.jl).

```@index
```

```@autodocs
Modules = [SVMakieRecipes]
```
