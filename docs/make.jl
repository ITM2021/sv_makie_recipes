using SVMakieRecipes
using Documenter

DocMeta.setdocmeta!(SVMakieRecipes, :DocTestSetup, :(using SVMakieRecipes); recursive=true)

makedocs(;
    modules=[SVMakieRecipes],
    authors="Matthew Camp",
    repo="https://github.com/mcamp/SVMakieRecipes.jl/blob/{commit}{path}#{line}",
    sitename="SVMakieRecipes.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mcamp.gitlab.io/SVMakieRecipes.jl",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
