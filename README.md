# SVMakieRecipes

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://mcamp.gitlab.io/SVMakieRecipes.jl/dev)
[![Build Status](https://github.com/mcamp/SVMakieRecipes.jl/badges/master/pipeline.svg)](https://github.com/mcamp/SVMakieRecipes.jl/pipelines)
[![Coverage](https://github.com/mcamp/SVMakieRecipes.jl/badges/master/coverage.svg)](https://github.com/mcamp/SVMakieRecipes.jl/commits/master)
[![Coverage](https://codecov.io/gh/mcamp/SVMakieRecipes.jl/branch/master/graph/badge.svg)](https://codecov.io/gh/mcamp/SVMakieRecipes.jl)
