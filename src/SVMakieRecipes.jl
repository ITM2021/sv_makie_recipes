module SVMakieRecipes

using Makie
using AbstractPlotting
import AbstractPlotting: plot!, @recipe, Attributes, poly!, Observables, Scene, set_theme!
using SpatialVoting
using GeometryBasics
using Colors

@recipe(SVPlot,sv,colorscheme) do scene
           Attributes(
               strokewidth           = 0.25,
               strokecolor           = :white,
                rowgap               = 5,
                colgap               = 5,
                fontsize             = 20,
                backgroundcolor      = :black,
                Axis                 = (
                    xgridcolor       = :white,
                    ygridcolor       = :white,
                    backgroundcolor  = :black,
                    xgridwidth       = 0.35,
                    ygridwidth       = 0.35,
                    xtickalign       = 0,
                    ytickalign       = 0,
                    xtickcolor       = :white,
                    ytickcolor       = :white,
                    bottomspinecolor = :white,
                    topspinecolor    = :white,
                    leftspinecolor   = :white,
                    rightspinecolor  = :white,
                    xlabelcolor      = :white,
                    ylabelcolor      = :white,
                    xlabelsize       = 24,
                    ylabelsize       = 24,
                    xticklabelcolor  = :white,
                    yticklabelcolor  = :white,
                    xgridvisible     = true,
                    ygridvisible     = true,
                ),
           )
       end

function AbstractPlotting.plot!(sv::SVPlot{<:Tuple{AbstractVector{<:SVCell}, AbstractVector{<:AbstractRGB}}})
    colors = sv[2]
    svcells  = sv[1]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])

    function update_plot(svcells::AbstractVector{<:SVCell},colors::AbstractVector{<:AbstractRGB})
        empty!(cells[])
        empty!(cell_colors[])
Threads.@threads        for (svcell, color) in zip(svcells,colors)
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end
    Observables.onany(update_plot, svcells, colors)
    update_plot(svcells[],colors[])
    AbstractPlotting.set_theme!(
        rowgap          = sv[:rowgap],
        colgap          = sv[:colgap],
        fontsize        = sv[:fontsize],
        backgroundcolor = sv[:backgroundcolor],
        Axis            = sv[:Axis],
    )
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor],aspect_ratio=:equal)
    # poly!(sv)
    sv
end

function AbstractPlotting.plot!(sv::SVPlot{<:Tuple{AbstractVector{<:SVCell}, SVColorScheme}})
    colors = sv[2]
    svcells  = sv[1]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])

    function update_plot(svcells::AbstractVector{<:SVCell},colors::SVColorScheme)
        empty!(cells[])
        empty!(cell_colors[])
Threads.@threads        for svcell in svcells
            color = colors[svcell.count]
            if typeof(color) == Symbol
                color = parse(Colorant,color)
            end
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end
    Observables.onany(update_plot, svcells, colors)
    update_plot(svcells[],colors[])
    AbstractPlotting.set_theme!(
        rowgap          = sv[:rowgap],
        colgap          = sv[:colgap],
        fontsize        = sv[:fontsize],
        backgroundcolor = sv[:backgroundcolor],
        Axis            = sv[:Axis],
    )
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor],aspect_ratio=:equal)
    sv
end

function AbstractPlotting.plot!(sv::SVPlot{<:Tuple{SVGrid,SVColorScheme}})
    svgrid  = sv[1]
    svcolors = sv[2]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])

    function update_plot(grid::SVGrid,colors::SVColorScheme)
        empty!(cells[])
        empty!(cell_colors[])

        # for svcell in filter(g-> typeof(g)==SVCell,grid.grid)
        for ix=1:length(grid.grid)
            svcell = grid.grid[ix]
            if !isdefined(svcell,1)
                #TODO: Fix for large things
                xy = CartesianIndices((length(grid.x),length(grid.y)))[ix]
                x,y = xy[1],xy[2]
                parent_key = grid.parent
                svcell = SVCell([],nothing,"$parent_key.$ix",grid.x,grid.y,0)
            end
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            color = colors[svcell.count]
            if typeof(color) == Symbol
                color = parse(Colorant,color)
            end
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end
    Observables.onany(update_plot, svgrid, svcolors)
    update_plot(svgrid[],svcolors[])
    AbstractPlotting.set_theme!(
        rowgap          = sv[:rowgap],
        colgap          = sv[:colgap],
        fontsize        = sv[:fontsize],
        backgroundcolor = sv[:backgroundcolor],
        Axis            = sv[:Axis],
    )
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor],aspect_ratio=:equal)
    sv
end

function AbstractPlotting.plot!(sv::SVPlot{<:Tuple{DynamicSVGrid,SVData,SVColorScheme}})
    dysvgrid  = sv[1]
    svdata    = sv[2]
    svcolors  = sv[3]
    
    cells = Node(Rect2D{Float64}[])
    cell_colors = Node(RGB{Float64}[])


    function update_plot(dy::DynamicSVGrid,data::SVData,colors::SVColorScheme)
        empty!(cells[])
        empty!(cell_colors[])

        for key=keys(data)
            parent_grid_key = SpatialVoting.get_parent_keyval(key)
            if parent_grid_key == ""
                current_colors = SVColorScheme(minimum(dy),maximum(dy);colors=colors.colors)
            else
                current_colors = SVColorScheme(minimum(dy[parent_grid_key]),maximum(dy[parent_grid_key]);colors=colors.colors)
            end
            svcell = dy[key]
            color = colors[svcell.count]
            if typeof(color) == Symbol
                color = parse(Colorant,color)
            end
            ll_corner = lower_left_corner(svcell)
            c = corners(svcell)
            x_size = abs(diff(collect(Set(c[1])))[1])
            y_size = abs(diff(collect(Set(c[2])))[1])
            cell = GeometryBasics.Rect(Vec(Float64(ll_corner.x),Float64(ll_corner.y)),Vec(Float64(x_size),Float64(y_size)))
            push!(cells[],cell)
            push!(cell_colors[],color)
        end
    end

    Observables.onany(update_plot, dysvgrid, svdata, svcolors)
    update_plot(dysvgrid[],svdata[],svcolors[])
    # @show cells.val[1], cell_colors.val[1]
    AbstractPlotting.set_theme!(
        rowgap          = sv[:rowgap],
        colgap          = sv[:colgap],
        fontsize        = sv[:fontsize],
        backgroundcolor = sv[:backgroundcolor],
        Axis            = sv[:Axis],
    )
    poly!(sv,cells,color=cell_colors,strokewidth=sv[:strokewidth],strokecolor=sv[:strokecolor])
    sv
end


export plot!
end
